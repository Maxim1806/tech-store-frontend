// eslint-disable-next-line nuxt/no-cjs-in-config
module.exports = {
  mode: 'universal',
  env: {
    SERVER_PATH: ''
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Tech Store',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Nikita & Maxim e-commerce training project'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: {color: '#3B8070'},
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    postcss: {
      plugins: {
        'postcss-flexbugs-fixes': process.env.NODE_ENV === 'development' ? false : {},
        'postcss-mq-keyframes': process.env.NODE_ENV === 'development' ? false : {}
      },
      preset: {
        stage: 2,
        autoprefixer: process.env.NODE_ENV === 'development' ? false : {
          cascade: false,
          flexbox: 'no-2009'
        }
      }
    }
  },

  router: {
    prefetchLinks: false
  },

  buildModules: [
    '@nuxtjs/eslint-module',
    [
      '@nuxtjs/router',
      {
        path: 'router',
        fileName: 'index.js'
      }
    ]
  ],

  modules: [
    '~/modules/routes.js',
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    [
      'nuxt-vuex-localstorage', {
      mode: 'debug',
      localStorage: [
        'cart',
        'favorite'
      ]
    }
    ]
  ],

  // All styles
  css: [
    '@/assets/scss/index.scss'
  ],

  // Plugins to load before mounting the App

  plugins: [
    '~/plugins/axios.js'
  ],

  styleResources: {
    scss: [
      '@/assets/scss/utils/variables.scss',
      '@/assets/scss/utils/mixins.scss'
    ]
  },

  server: {
    // port: process.env.NODE_ENV === 'development' ? 1489 : 1311
  },

  // Axios module configuration

  axios: {
    baseURL: 'https://tech-store-app.herokuapp.com/api/'
    // credentials: true
  }
}

