import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const index = () => import('~/pages/home/index').then(m => m.default || m)
const pageProduct = () => import('~/pages/pageProduct/pageProduct').then(m => m.default || m)
const pageFavorite = () => import('~/pages/pageFavorite/pageFavorite').then(m => m.default || m)
const pageCart = () => import('~/pages/pageCart/pageCart').then(m => m.default || m)

export function createRouter () {
  return new VueRouter({
    mode: 'history',
    routes: [
      {
        path: '/',
        component: index,
        name: 'index'
      },
      {
        path: '/product/:id',
        component: pageProduct,
        name: 'pageProduct'
      },
      {
        path: '/favorite',
        component: pageFavorite,
        name: 'pageFavorite'
      },
      {
        path: '/cart',
        component: pageCart,
        name: 'pageCart'
      }
    ]
  })
}
