export const state = () => ({
  list: [],
  favoriteCount: 0
})

export const mutations = {
  SET_ADD_T0_FAVORITE_PRODUCT (state, info) {
    let found = state.list.find(product => product._id === info._id)

    if (!found) {
      state.list.push(info)
      state.favoriteCount++
    }
  },

  SET_REMOVE_FROM_FAVORITE_PRODUCT (state, info) {
    let index = state.list.indexOf(info)

    if (index > -1) {
      state.favoriteCount--
      state.list.splice(index, 1)
    }
  },

  SET_ADD_T0_FAVORITE_PRODUCT_PAGE (state, product) {
    let foundProduct = state.list.find(goods => goods._id === product._id)
    if (!foundProduct) {
      state.list.push(product)
      state.favoriteCount++
    }
  },

  SET_REMOVE_FROM_FAVORITE_PRODUCT_PAGE (state, product) {
    let index = state.list.indexOf(product)

    if (index > -1) {
      state.favoriteCount--
      state.list.splice(index, 1)
    }
  }
}

export const actions = {}

export const getters = {}